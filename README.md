# ansible-role-alertmanager

Ansible role for deploying the Alertmanager binary for Prometheus 

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://gitlab.com/adieperi/ansible-role-alertmanager.git
  scm: git
  version: main
  name: ansible-role-alertmanager
```

### Download the role

```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.10 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `alertmanager_version` | latest | [version](https://github.com/alertmanager/alertmanager/tags) | Alertmanager version. |
| `alertmanager_log_level` | info | debug, info, warn, error | Alertmanager log level. |
| `alertmanager_log_format` | logfmt | logfmt, json | Alertmanager log format. |
| `alertmanager_listen_address` | ':9093' |  | Address to listen on for UI, API, and telemetry. |
| `alertmanager_external_url` | 'http://{{ ansible_default_ipv4.address }}{{ alertmanager_listen_address }}' |  | The URL under which alertmanager is externally reachable. |
| `alertmanager_extra_opts` | [] |  | Alertmanager extra options. |
| `alertmanager_configuration` | [] |  | Alertmanager [configuration](https://prometheus.io/docs/alerting/latest/configuration/) |
| `alertmanager_web_configuration` | [] |  | Alertmanager [web configuration](https://prometheus.io/docs/alerting/latest/https/). |

## Example Playbook

```yaml
---
- hosts: all
  tasks:
    - name: Include ansible-role-alertmanager
      include_role:
        name: ansible-role-alertmanager
      vars:
        alertmanager_version: '0.22.2'
        alertmanager_extra_opts:
          - --data.retention=24h

        alertmanager_web_configuration:
          tls_server_config:
            cert_file: /etc/ssl/prometheus.crt
            key_file: /etc/ssl/prometheus.key
          basic_auth_users:
            admin: Pa$$w0rd

        alertmanager_configuration:
          global:
            resolve_timeout: 1m
            smtp_from: 'no-reply@domain.net'
            smtp_smarthost: mail.domain.net:587
            smtp_auth_username: 'no-reply@domain.net'
            smtp_auth_password: 'Pa$$w0rd'

          route:
            receiver: 'email-notifications'

          receivers:
            - name: 'email-notifications'
            email_configs:
              - to: monitoring@domain.net
              send_resolved: true
```

## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
